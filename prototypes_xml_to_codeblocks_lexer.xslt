﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
  <xsl:output method="xml"
	version="1.0"
	doctype-public="CodeBlocks_lexer_properties"
	doctype-system=""
	indent="yes"
	media-type="text/xml"
    encoding="UTF-8"/>
	
	<xsl:strip-space elements="*"/>

<xsl:template match="/">
<!-- Styles and Header Data -->
<!-- Customise to suit -->
<CodeBlocks_lexer_properties>
	<!-- Scintilla Index #3 = C/C++ format -->
	<Lexer name="12D macro"
			index="3"		
			filemasks="*.4dm,*.c;*.h">
		<Style name="Default"
				index="0"
				fg="0,0,0"
				bg="255,255,255"
				bold="0"
				italics="0"
				underlined="0"/>
		<Style name="Comment (normal)"
				index="1,2"
				fg="160,160,160"/>
		<Style name="Comment (documentation)"
				index="3,15"
				fg="128,128,255"
				bold="1"/>
		<Style name="Comment keyword (documentation)"
				index="17"
				fg="0,128,128"/>
		<Style name="Comment keyword error (documentation)"
				index="18"
				fg="128,0,0"/>
		<Style name="Number"
				index="4"
				fg="240,0,240"/>
		<Style name="Keyword"
				index="5"
				fg="0,0,160"
				bold="1"/>
		<Style name="User keyword"
				index="16"
				fg="0,160,0"
				bold="1"/>
		<Style name="String"
				index="6,12"
				fg="0,0,255"/>
		<Style name="Character"
				index="7"
				fg="224,160,0"/>
		<Style name="UUID"
				index="8"
				fg="0,0,0"/>
		<Style name="Preprocessor"
				index="9"
				fg="0,160,0"/>
		<Style name="Operator"
				index="10"
				fg="255,0,0"/>
		<Style name="Selection"
				index="-99"
				bg="192,192,192"/>
		<Style name="Active line"
				index="-98"
				bg="255,255,160"/>
		<Style name="Breakpoint line"
				index="-2"
				bg="255,160,160"/>
		<Style name="Debugger active line"
				index="-3"
				bg="160,160,255"/>
		<Style name="Compiler error line"
				index="-4"
				bg="255,128,0"/>
		<Style name="Matching brace highlight"
				index="34"
				bg="128,255,255"
				bold="1"/>
		<Style name="No matching brace highlight"
				index="35"
				fg="255,255,255"
				bg="255,0,0"
				bold="1"/>
		<Keywords>
			<xsl:comment>Primary keywords and identifiers</xsl:comment>
			<Set index="0"
				value="break case char continue default do double else float for goto if int integer long real return short switch void while auto class const delete enum extern friend inline new operator private protected public register signed sizeof static struct template this throw try typedef union unsigned virtual volatile" 
			/>
			<xsl:comment>Secondary keywords and identifiers</xsl:comment>
				<xsl:call-template name="datatypes" />
			<xsl:comment>Documentation comment keywords</xsl:comment>
			
			<xsl:comment>Unused</xsl:comment>
			
			<xsl:comment>Global classes and typedefs</xsl:comment>
				<xsl:apply-templates select="MacroCalls" />
				
		</Keywords>
		<SampleCode value="lexer_12dm.sample"
				breakpoint_line="20"
				debug_line="22"
				error_line="23" />
	</Lexer>
</CodeBlocks_lexer_properties>

</xsl:template>
	
	
<xsl:template name="datatypes">
	<xsl:element name="Set">
		<xsl:attribute name="index">1</xsl:attribute>
		<xsl:attribute name="value">
			<xsl:for-each select="//Type[not(.=preceding::Type)]">
				<!-- Can uncomment the line below to enable sorting, likely to take longer to process -->
				<!-- <xsl:sort select="." order="ascending"/> -->
				<xsl:value-of select="."/>
				<xsl:if test="position()!=last()">
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:attribute>
	</xsl:element>
</xsl:template>
	
<xsl:template match="MacroCalls">
	<xsl:element name="Set">
		<xsl:attribute name="index">4</xsl:attribute>
		<xsl:attribute name="value">
			<xsl:for-each select="//MacroCall/Name[not(.=preceding::Name)]">
				<!-- Can uncomment the line below to enable sorting, likely to take longer to process -->
				<!-- <xsl:sort select="." order="ascending"/> -->
				<xsl:value-of select="."/>
				<xsl:if test="position()!=last()">
					<xsl:text> </xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:attribute>
	</xsl:element>
</xsl:template>

</xsl:stylesheet>