@ECHO OFF
SETLOCAL

REM ========= GENERATE CODE::BLOCKS LEXER FOR 12DPL ==========
REM Generates a new list of function prototypes for the 12d 
REM Model Programming Language (12dPL) and creates a lexer
REM file based on those functions for the Code::Blocks IDE.
REM 

CLS
ECHO.
ECHO ##########################################################
ECHO #                                                        #
ECHO #            PROTOTYPES TO CODEBLOCKS LEXER              #
ECHO #                                                        #
ECHO ##########################################################
ECHO.

REM 12d Model Installation directory
REM Modify to suit your version and location
REM This location is used to specify the locations of the 12dPL
REM Preprocessor (cc4d.exe) which generates the prototypes files
REM and the XML Transformer which applys an XSLT to the XML to
REM generate the output lexer file. 
SET TDINSTALL=C:\Program Files\12d\12dmodel\14.00

REM 12dPL Compiler
REM Used to generate the prototypes files
SET CC4D=%TDINSTALL%\nt.x64\cc4d.exe

REM XML Transform executable
REM Used to apply an XSLT to the prototypes XML file to output 
REM the Code::Blocks lexer
SET XMLT=%TDINSTALL%\Xml\XmlTransform.exe

REM Prototypes files to generate
REM Prototypes files to generate- text and XML formats
SET PROTO=lexer_prototypes.txt
SET PROTOXML=%PROTO%.xml

REM XML Stylesheet
REM XSLT to use for converting prototypes to lexer file
SET XSL=prototypes_xml_to_codeblocks_lexer.xslt

REM Output Lexer File
REM Name of the lexer file to be generated
SET OUTPUT=lexer_12dPL.xml

REM Sample Lexer File
REM Not created by this process, but included with the Lexer file
REM to demonstrate the Syntax Highlighting within Code::Blocks
REM This should contain a small sample of 12dPL code with various
REM language features to demonstrate the application of the lexer
SET SAMPLE=lexer_12dPL.sample

REM Target Lexers Directory
REM The lexer files will be copied to this location.
REM If using the Program Files location, you may need to run this 
REM batch file with Administrator rights.
SET TARGET=%appdata%\CodeBlocks\share\codeblocks\lexers
SET TARGET=.

IF NOT EXIST "%CC4D%" (
	ECHO.
	ECHO 12dPL Compiler not found. Expected at- %CC4D%
	ECHO Cannot generate prototypes file
	ECHO.
	PAUSE
	EXIT /B 1
) ELSE (
	ECHO.
	ECHO 12dPL Compiler found- %CC4D%
)

IF NOT EXIST "%XMLT%" (
	ECHO.
	ECHO XML Transform executable not found. Expected at- %XMLT%
	ECHO Cannot transform prototypes to lexer file
	ECHO.
	PAUSE
	EXIT /B 2
) ELSE (
	ECHO XML Transform found- %XMLT%
)

IF NOT EXIST "%XSL%" (
	ECHO.
	ECHO Lexer XSLT not found. Expected at- %XSL%
	ECHO Cannot transform prototypes to lexer file
	ECHO.
	PAUSE
	EXIT /B 3
) ELSE (
	ECHO Lexer XSLT found- %XSL%
)
ECHO.
ECHO Generating prototypes for CodeBlocks lexer...
"%CC4D%" -list "%PROTO%"
IF ERRORLEVEL 1 (
	ECHO.
	ECHO 12dPL Compiler returned an error- %ERRORLEVEL%
	ECHO Problem generating prototypes file- %PROTO%
	ECHO.
	PAUSE
	EXIT /B 4
)

IF NOT EXIST "%PROTOXML%" (
	ECHO.
	ECHO Prototypes XML file not found. Expected at- %PROTOXML%
	ECHO Were there any errors in generating the prototypes?
	ECHO.
	PAUSE
	EXIT /B 5
) ELSE (
	ECHO Prototypes XML created- %PROTOXML%
)

:DO_XSLT
ECHO Transforming prototypes XML to CodeBlocks lexer...
"%XMLT%" "%PROTOXML%" "%XSL%" "%OUTPUT%"
IF ERRORLEVEL 1 (
	ECHO.
	ECHO XML Transform returned an error- %ERRORLEVEL%
	ECHO Problem transforming Prototypes XML to CodeBlocks lexer
	ECHO.
	ECHO XML Transform:  %XMLT%
	ECHO Prototypes XML: %PROTOXML%
	ECHO CodeBlocks XSL: %XSL%
	ECHO Output Lexer:   %OUTPUT%
	ECHO.
	PAUSE
	EXIT /B 6
)

IF NOT EXIST "%OUTPUT%" (
	ECHO.
	ECHO CodeBlocks Lexer file not found. Expected at- %OUTPUT%
	ECHO.
	PAUSE
	EXIT /B 7
)

IF ["%TARGET%"] EQU [] (
	ECHO.
	ECHO Target directory is blank or unspecified.
	GOTO FINISHED
)
IF ["%TARGET%"] EQU [""] (
	ECHO.
	ECHO Target directory is blank or unspecified.
	GOTO FINISHED
)
IF "%TARGET%" EQU "." (
	ECHO.
	ECHO Target directory is current directory.
	GOTO FINISHED
)
ECHO.
ECHO Copying lexer files to CodeBlocks location...
COPY "%OUTPUT%" "%TARGET%\%OUTPUT%"
COPY "%SAMPLE%" "%TARGET%\%SAMPLE%"

:FINISHED
ECHO Finished.
PAUSE
EXIT /B 0


:END
